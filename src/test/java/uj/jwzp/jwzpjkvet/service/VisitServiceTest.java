package uj.jwzp.jwzpjkvet.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import uj.jwzp.jwzpjkvet.DateNow;
import uj.jwzp.jwzpjkvet.exception.*;
import uj.jwzp.jwzpjkvet.model.*;
import uj.jwzp.jwzpjkvet.repository.VisitRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class VisitServiceTest {

    private static final Person PERSON1 = new Person(
            1,
            "Jan",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "342059374",
            "jankowalski@onet.pl"
    );

    private static final Pet PET1 = new Pet(
            1,
            "Łatek",
            PetType.DOG,
            "2019-02-12",
            null,
            1,
            null);

    private static final Visit VISIT1 = new Visit(
            1,
            PET1,
            LocalDate.parse("2020-05-28"),
            LocalTime.parse("09:00:00"),
            LocalTime.parse("09:30:00"),
            "30",
            null,
            VisitStatus.APPOINTMENT);

    private static final Visit VISIT2 = new Visit(
            2,
            PET1,
            LocalDate.parse("2020-05-28"),
            LocalTime.parse("10:00:00"),
            LocalTime.parse("10:45:00"),
            "45",
            null,
            VisitStatus.APPOINTMENT);

    private static final Visit VISIT3 = new Visit(
            2,
            null,
            LocalDate.parse("2020-05-29"),
            LocalTime.parse("10:00:00"),
            LocalTime.parse("11:00:00"),
            "60",
            null,
            VisitStatus.NOTCAME);


    @MockBean
    private DateNow dateNow;

    @Mock
    private PetsService petsService;

    @Mock
    private VisitRepository visitRepository;

    @InjectMocks
    private VisitService visitService;

    @Test
    void getVisitsReturnVisit() {
        //given
        given(visitRepository.findById(1)).willReturn(java.util.Optional.of(VISIT1));
        //when
        Visit result = visitService.getVisit(1);
        //then
        assertThat(result.equals(VISIT1));
    }

    @Test
    void getVisitByIdVisitDoesNotExist() {
        //given
        given(visitRepository.findById(1)).willReturn(Optional.empty());
        //when
        //then
        Assertions.assertThrows(VisitNotFoundException.class, () -> {
            visitService.getVisit(1);
        });
    }

    @Test
    void getAllByDateReturnListOfVisits() {
        //given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitRepository.findAllByDate(LocalDate.parse("2020-05-28"))).willReturn(visits);
        //when
        List<Visit> result = visitService.getAllByDate(LocalDate.parse("2020-05-28"));
        //then
        assertThat(result.equals(visits));
    }

    @Test
    void getAllByDateWheresStatusReturnListOfVisits() {
        //given
        List<Visit> visits = List.of(VISIT3);
        given(visitRepository.findAllByDateAndStatus(LocalDate.parse("2020-05-28"), VisitStatus.NOTCAME)).willReturn(visits);
        //when
        List<Visit> result = visitService.getAllByDateWheresStatus(LocalDate.parse("2020-05-28"), VisitStatus.NOTCAME);
        //then
        assertThat(result.equals(visits));
    }

    @Test
    void getAllByPersonIdReturnListOfVisits() {
        //given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        List<Pet> pets = List.of(PET1);
        given(petsService.findAllByOwnerId(1)).willReturn(pets);
        given(visitRepository.getAllByPetId(1)).willReturn(visits);
        //when
        List<Visit> result = visitService.getAllByPersonId(1);
        //then
        assertThat(result.equals(visits));
    }

    @Test
    void getAllByPetIdReturnListOfVisits() {
        //given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitRepository.getAllByPetId(1)).willReturn(visits);
        //when
        List<Visit> result = visitService.getAllByPetId(1);
        //then
        assertThat(result.equals(visits));
    }

    @Test
    void checkValidDatereturnTrue() throws InvalidDateException {
        //given
        LocalDate dateNow = LocalDate.parse("2020-05-28");
        LocalDate date = LocalDate.parse("2020-05-29");
        //when
        boolean result = visitService.validDate(dateNow, date);
        //then
        assertThat(result == true);
    }

    @Test
    void checkValidDateReturnThrowExceptionWhenSaturady() {
        //given
        LocalDate dateNow = LocalDate.parse("2020-05-28");
        LocalDate date = LocalDate.parse("2020-05-30");
        //when
        //then
        Assertions.assertThrows(InvalidDateException.class, () -> {
            visitService.validDate(dateNow, date);
        });
    }

    @Test
    void checkValidDateReturnThrowExceptionWhenDateBeforeActualDate() {
        //given
        LocalDate dateNow = LocalDate.parse("2020-05-28");
        LocalDate date = LocalDate.parse("2020-05-27");
        //when
        //then
        Assertions.assertThrows(InvalidDateException.class, () -> {
            visitService.validDate(dateNow, date);
        });
    }

    @Test
    void checkValidTimeReturnTrue() throws InvalidTimeException {
        //given
        LocalTime timeStart = LocalTime.parse("12:00:00");
        LocalTime timeEnd = LocalTime.parse("12:30:00");
        //when
        boolean result = visitService.validTime(timeStart, timeEnd);
        //then
        assertThat(result == true);
    }

    @Test
    void checkValidTimeReturnThrowExceptionWhenOutOfRange() {
        //given
        LocalTime timeStart = LocalTime.parse("21:00:00");
        LocalTime timeEnd = LocalTime.parse("21:30:00");
        //when
        //then
        Assertions.assertThrows(InvalidTimeException.class, () -> {
            visitService.validTime(timeStart, timeEnd);
        });
    }

    @Test
    void checkConflictTerminReturnTrue() {
        //given
        LocalDate date = LocalDate.parse("2020-05-28");
        LocalTime timeStart = LocalTime.parse("09:30:00");
        LocalTime timeEnd = LocalTime.parse("09:45:00");
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitService.getAllByDate(date)).willReturn(visits);
        //when
        boolean result = visitService.isConflictedTermin(date, timeStart, timeEnd);
        //then
        assertThat(result == false);
    }

    @Test
    void checkConflictTerminReturnThrowExceptionWhenConflict() {
        //given
        LocalDate date = LocalDate.parse("2020-05-28");
        LocalTime timeStart = LocalTime.parse("09:30:00");
        LocalTime timeEnd = LocalTime.parse("10:15:00");
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitService.getAllByDate(date)).willReturn(visits);
        //when
        boolean result = visitService.isConflictedTermin(date, timeStart, timeEnd);
        //then
        assertThat(result == true);
    }
}