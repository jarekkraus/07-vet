package uj.jwzp.jwzpjkvet.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uj.jwzp.jwzpjkvet.exception.PetNotFoundException;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.PetType;
import uj.jwzp.jwzpjkvet.repository.PetsRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;


@ExtendWith(MockitoExtension.class)
class PetsServiceTest {

    private static final Pet PET1 = new Pet(
            1,
            "Łatek",
            PetType.DOG,
            "2019-02-12",
            null,
            null,
            null);

    private static final Pet PET2 = new Pet(
            2,
            "Alfa",
            PetType.HORSE,
            "2018-01-12",
            null,
            null,
            null);

    @Mock
    private PetsRepository petsRepository;

    @InjectMocks
    private PetsService petsService;

    @Test
    void getAllPetsReturnListOfPets() {
        //given
        List<Pet> pets = List.of(PET1);
        given(petsRepository.findAll()).willReturn(pets);
        //when
        List<Pet> result = petsService.getAllPets();
        //then
        assertThat(result.equals(pets));
    }

    @Test
    void getPetByIdReturnPet() {
        //given
        given(petsRepository.findById(1)).willReturn(java.util.Optional.of(PET1));
        //when
        Pet result = petsService.getPet(1);
        //then
        assertThat(result.equals(PET1));
    }

    @Test
    void getPetByIdPetDoesNotExist() {
        //given
        given(petsRepository.findById(1)).willReturn(Optional.empty());
        //when
        //then
        Assertions.assertThrows(PetNotFoundException.class, () -> {
            petsService.getPet(1);
        });
    }

    @Test
    void addPetReturnNewPet() {
        //given
        given(petsRepository.save(PET1)).willReturn(PET1);
        //when
        Pet result = petsService.addPet(PET1);
        //them
        assertThat(result.equals(PET1));
    }

    @Test
    void updatePetReturnUpdatedPet() {
        //given
        Pet pet = new Pet(
                1,
                "Reksio",
                PetType.DOG,
                "2019-02-12",
                null,
                null,
                null);
        given(petsRepository.findById(1)).willReturn(Optional.of(PET1));
        given(petsRepository.save(pet)).willReturn(pet);
        //when
        Pet result = petsService.updatePet(1, pet);
        //then
        assertThat(result.equals(pet));
    }

    @Test
    void deletePet() {
        //given
        given(petsRepository.findById(1)).willReturn(Optional.of(PET1));
        //when
        //them
        petsService.deletePet(1);
    }

    @Test
    void findAllPetsByOwnerIdReturnListOfPets() {
        //given
        given(petsRepository.findAllByOwnerId(1)).willReturn(List.of(PET1, PET2));
        //when
        List<Pet> result = petsService.findAllByOwnerId(1);
        //them
        assertThat(result.contains(PET1));
    }

    @Test
    void findAllPetsByOwnerIdReturnEmptyList() {
        //given
        given(petsRepository.findAllByOwnerId(1)).willReturn(Collections.emptyList());
        //when
        List<Pet> result = petsService.findAllByOwnerId(1);
        //them
        assertThat(result.isEmpty());
    }
}