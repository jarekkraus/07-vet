package uj.jwzp.jwzpjkvet.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import uj.jwzp.jwzpjkvet.exception.PersonNotFoundException;
import uj.jwzp.jwzpjkvet.model.Person;
import uj.jwzp.jwzpjkvet.model.PersonWithPets;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.PetType;
import uj.jwzp.jwzpjkvet.repository.PeopleRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class PeopleServiceTest {

    private static final Person PERSON1 = new Person(
            1,
            "Jan",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "342059374",
            "jankowalski@onet.pl"
    );

    private static final Person PERSON2 = new Person(
            2,
            "Marek",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "642859374",
            "marekkowalski@onet.pl"
    );

    @Mock
    private PeopleRepository peopleRepository;

    @InjectMocks
    private PeopleService peopleService;

    @Mock
    private PetsService petsService;

    @Test
    void getAllPeopleReturnList() {
        //given
        List<Person> people = List.of(PERSON1, PERSON2);
        given(peopleRepository.findAll()).willReturn(people);
        //when
        List<Person> result = peopleService.getAllPeople();
        //then
        assertThat(result.equals(people));
    }

    @Test
    void getPersonByIdReturnPerson() {
        //given
        given(peopleRepository.findById(1)).willReturn(java.util.Optional.of(PERSON1));
        //when
        Person result = peopleService.getPerson(1);
        //then
        assertThat(result.equals(PERSON1));
    }

    @Test
    void getPersonByIdPersonDoesNotExist() {
        //given
        given(peopleRepository.findById(1)).willReturn(Optional.empty());
        //when
        //then
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            peopleService.getPerson(1);
        });
    }

    @Test
    void addPersonReturnPerson() {
        //given
        given(peopleRepository.save(PERSON1)).willReturn(PERSON1);
        //when
        Person result = peopleService.addPerson(PERSON1);
        //them
        assertThat(result.equals(PERSON1));
    }

    @Test
    void updatePersonReturnPerson() {
        //given
        Person person = new Person(
                1,
                "Janusz",
                "Kowalski",
                "Kamieńskiego 11, 30-644 Kraków",
                "342059374",
                "jankowalski@onet.pl"
        );
        given(peopleRepository.findById(1)).willReturn(Optional.of(PERSON1));
        given(peopleRepository.save(person)).willReturn(person);
        //when
        Person result = peopleService.updatePerson(1, person);
        //then
        assertThat(result.equals(person));
    }

    @Test
    void deletePerson() {
        //given
        given(peopleRepository.findById(1)).willReturn(Optional.of(PERSON1));
        //when
        //them
        peopleService.deletePerson(1);
    }

    @Test
    void getPersonWithPetsReturnEmptyList() {
        //given
        given(peopleRepository.findById(1)).willReturn(Optional.of(PERSON1));
        given(petsService.findAllByOwnerId(1)).willReturn(Collections.emptyList());
        //when
        PersonWithPets result = (PersonWithPets) peopleService.getPersonWithPets(1);
        //then
        assertThat(result.pets().isEmpty());
    }

    @Test
    void getPersonWithPetsReturnListOfOnePet() {
        //given
        Pet pet = new Pet(
                1,
                "Reksio",
                PetType.DOG,
                "2019-02-12",
                null,
                1,
                null);
        given(peopleRepository.findById(1)).willReturn(Optional.of(PERSON1));
        given(petsService.findAllByOwnerId(1)).willReturn(List.of(pet));
        //when
        PersonWithPets result = (PersonWithPets) peopleService.getPersonWithPets(1);
        //then
        assertThat(result.pets().contains(pet));
    }
}