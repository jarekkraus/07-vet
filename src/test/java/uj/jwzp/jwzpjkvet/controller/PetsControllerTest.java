package uj.jwzp.jwzpjkvet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uj.jwzp.jwzpjkvet.exception.PetNotFoundException;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.PetType;
import uj.jwzp.jwzpjkvet.service.PetsService;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@WebMvcTest(PetsController.class)
class PetsControllerTest {

    private static final Pet PET1 = new Pet(
            1,
            "Łatek",
            PetType.DOG,
            "2019-02-12",
            null,
            null,
            null);

    private static final Pet PET2 = new Pet(
            2,
            "Alfa",
            PetType.HORSE,
            "2018-01-12",
            null,
            null,
            null);

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PetsService petsService;

    @Test
    void getAllPetsReturnPets() throws Exception {
        // given
        List<Pet> pets = List.of(PET1, PET2);
        given(petsService.getAllPets()).willReturn(pets);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/pets")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(pets.toString()));
    }

    @Test
    void getPetReturnPet() throws Exception {
        // given
        given(petsService.getPet(1)).willReturn(PET1);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/pets/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(PET1.toString()));
    }

    @Test
    void getPetByIdPetDoesNotExistsReturnNotFound() throws Exception {
        // given
        given(petsService.getPet(1)).willThrow(new PetNotFoundException(1));
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/pet/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void addPetReturnCreated() throws Exception {
        // given
        given(petsService.addPet(PET1)).willReturn(PET1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/pets")
                .content(asJsonString(new Pet(1, "Alex", PetType.DOG, "2020-02-01", null, null, null)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    void addPetWithWrongDateFormatReturnBadRequest() throws Exception {
        // given
        given(petsService.addPet(PET1)).willReturn(PET1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/pets")
                .content(asJsonString(new Pet(1, "Alex", PetType.DOG, "2020/02/01", null, null, null)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void addPetWithOnlyYearDateFormatReturnCreated() throws Exception {
        // given
        given(petsService.addPet(PET1)).willReturn(PET1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/pets")
                .content(asJsonString(new Pet(1, "Alex", PetType.DOG, "2020", null, null, null)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    void updatePetReturnUpdatedPet() throws Exception {
        // given
        Pet pet = new Pet(
                1,
                "Reksio",
                PetType.DOG,
                "2019-02-12",
                null,
                null,
                null);
        given(petsService.getPet(1)).willReturn(PET1);
        given(petsService.updatePet(1, pet)).willReturn(pet);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .put("/api/pets/1")
                .content(asJsonString(pet))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void deletePet() throws Exception {
        //given
        int petId = 1;
        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/pets/" + petId))
                .andReturn();
        //then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().contains("Pet with Id : 1 deleted successfully"));
    }

    @Test
    void assignOwnerReturnSuccess() throws Exception {
        //given
        int ownerId = 1;
        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .patch("/api/pets/1?ownerId=" + ownerId))
                .andReturn();
        //then
        assertThat(result.getResponse().getContentAsString().contains("Pet with Id : 1 assign to person with Id : 1 successfully"));
    }

    @Test
    void rejectOwnerReturnSuccess() throws Exception {
        //given
        int ownerId = 0;
        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .patch("/api/pets/1?ownerId=" + ownerId))
                .andReturn();
        //then
        assertThat(result.getResponse().getContentAsString().contains("Pet with Id : 1 reject person owner successfully"));
    }
}