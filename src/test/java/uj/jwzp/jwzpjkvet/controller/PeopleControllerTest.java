package uj.jwzp.jwzpjkvet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uj.jwzp.jwzpjkvet.exception.PersonNotFoundException;
import uj.jwzp.jwzpjkvet.model.Person;
import uj.jwzp.jwzpjkvet.model.PersonWithPets;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.PetType;
import uj.jwzp.jwzpjkvet.service.PeopleService;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@WebMvcTest(PeopleController.class)
class PeopleControllerTest {

    private static final Person PERSON1 = new Person(
            1,
            "Jan",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "342059374",
            "jankowalski@onet.pl"
    );

    private static final Person PERSON2 = new Person(
            2,
            "Marek",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "642859374",
            "marekkowalski@onet.pl"
    );

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PeopleService peopleService;

    @Test
    void getAllPeopleReturnPeople() throws Exception {
        // given
        List<Person> people = List.of(PERSON1, PERSON2);
        given(peopleService.getAllPeople()).willReturn(people);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/people")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(people.toString()));
    }

    @Test
    void getPersonReturnPerson() throws Exception {
        // given
        given(peopleService.getPerson(1)).willReturn(PERSON1);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/people/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(PERSON1.toString()));
    }

    @Test
    void getPersonWithPetsReturnPersonWithPets() throws Exception {
        // given
        boolean withPets = true;
        Pet pet = new Pet(
                1,
                "Reksio",
                PetType.DOG,
                "2019-02-12",
                null,
                null,
                null);
        PersonWithPets person = new PersonWithPets(PERSON1, List.of(pet));
        given(peopleService.getPersonWithPets(1)).willReturn(person);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/people/1?withPets=" + withPets)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().contains(pet.toString()));
    }

    @Test
    void getPersonByIdPersonDoesNotExistReturnNotFound() throws Exception {
        // given
        given(peopleService.getPerson(1)).willThrow(new PersonNotFoundException(1));
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/people/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void addPersonReturnCreated() throws Exception {
        // given
        given(peopleService.addPerson(PERSON1)).willReturn(PERSON1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/people")
                .content(asJsonString(new Person(1,
                                "Jan",
                                "Kowalski",
                                "Kamieńskiego 11, 30-644 Kraków",
                                "342059374",
                                "jankowalski@onet.pl")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    void addPersonWithoutAllFieldsReturnBadRequest() throws Exception {
        // given
        String json = "{\"firstName\": \"Jan\", \"lastName\": \"Kowalski\"}";
        given(peopleService.addPerson(PERSON1)).willReturn(PERSON1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/people")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(result.getResponse().getContentAsString().contains("Telephone may not be empty"));

    }

    @Test
    void addPersonReturnError() throws Exception {
        // given
        given(peopleService.addPerson(PERSON1)).willReturn(PERSON1);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/people")
                .content(asJsonString(null))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Test
    void updateOwnerReturnUpdatedPerson() throws Exception {
        // given
        Person updatedPerson = new Person(1,
                "Janusz",
                "Kowalski",
                "Kamieńskiego 11, 30-644 Kraków",
                "342059374",
                "jankowalski@onet.pl");
        given(peopleService.getPerson(1)).willReturn(PERSON1);
        given(peopleService.updatePerson(1, updatedPerson)).willReturn(updatedPerson);
        // when
        MvcResult result = mockMvc.perform( MockMvcRequestBuilders
                .put("/api/people/1")
                .content(asJsonString(updatedPerson))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    void deleteOwner() throws Exception {
        //given
        int personId = 1;
        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/people/" + personId))
                .andReturn();
        //then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().contains("Person with Id : 1 deleted successfully"));
    }


}