package uj.jwzp.jwzpjkvet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import uj.jwzp.jwzpjkvet.DateNow;
import uj.jwzp.jwzpjkvet.exception.ConflictDateException;
import uj.jwzp.jwzpjkvet.exception.InvalidDateException;
import uj.jwzp.jwzpjkvet.exception.InvalidTimeException;
import uj.jwzp.jwzpjkvet.model.*;
import uj.jwzp.jwzpjkvet.service.PeopleService;
import uj.jwzp.jwzpjkvet.service.PetsService;
import uj.jwzp.jwzpjkvet.service.VisitService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@WebMvcTest(VisitController.class)
class VisitControllerTest {

    private static final Person PERSON1 = new Person(
            1,
            "Jan",
            "Kowalski",
            "Kamieńskiego 11, 30-644 Kraków",
            "342059374",
            "jankowalski@onet.pl"
    );

    private static final Pet PET1 = new Pet(
            1,
            "Łatek",
            PetType.DOG,
            "2019-02-12",
            null,
            1,
            null);

    private static final Visit VISIT1 = new Visit(
            1,
            PET1,
            LocalDate.parse("2020-05-28"),
            LocalTime.parse("09:00:00"),
            LocalTime.parse("09:30:00"),
            "30",
            null,
            VisitStatus.APPOINTMENT);

    private static final Visit VISIT2 = new Visit(
            2,
            PET1,
            LocalDate.parse("2020-05-28"),
            LocalTime.parse("10:00:00"),
            LocalTime.parse("10:45:00"),
            "45",
            null,
            VisitStatus.APPOINTMENT);

    private static final Visit VISIT3 = new Visit(
            2,
            null,
            LocalDate.parse("2020-05-29"),
            LocalTime.parse("10:00:00"),
            LocalTime.parse("11:00:00"),
            "60",
            null,
            VisitStatus.NOTCAME);

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PeopleService peopleService;

    @MockBean
    private PetsService petsService;

    @MockBean
    private VisitService visitService;

    @Test
    void getVisitReturnVisit() throws Exception {
        // given
        given(visitService.getVisit(1)).willReturn(VISIT1);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/visits/1")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(VISIT1.toString()));
    }

    @Test
    void getAllByDateWheresStatusReturnListOfVisits() throws Exception {
        // given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        LocalDate date = LocalDate.parse("2020-05-28");
        given(visitService.getAllByDateWheresStatus(date, VisitStatus.APPOINTMENT)).willReturn(visits);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/visits?date=" + date.toString())
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(visits.toString()));
    }

    @Test
    void getAllPersonByIdVisitsReturnListOfVisits() throws Exception {
        // given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitService.getAllByPersonId(1)).willReturn(visits);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/people/1/visits")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(visits.toString()));
    }

    @Test
    void getAllPetsByIdVisitsReturnListOfVisits() throws Exception {
        // given
        List<Visit> visits = List.of(VISIT1, VISIT2);
        given(visitService.getAllByPetId(1)).willReturn(visits);
        // when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/pets/1/visits")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        // then
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(result.getResponse().getContentAsString().equals(visits.toString()));
    }
}