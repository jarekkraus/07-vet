package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonNotFoundException extends ResourceNotFoundException{

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonNotFoundException.class);

    public PersonNotFoundException(int id) {
        super("Could not find person with id " + id);
        LOGGER.error("Could not find person with id " + id);
    }
}
