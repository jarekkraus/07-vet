package uj.jwzp.jwzpjkvet.exception;

import java.time.LocalDateTime;

public class ErrorDetails {

    private final LocalDateTime timestamp;
    private final String message;

    public ErrorDetails(LocalDateTime timestamp, String message) {
        super();
        this.timestamp = timestamp;
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }
}
