package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PetNotFoundException extends ResourceNotFoundException{

    private static final Logger LOGGER = LoggerFactory.getLogger(PetNotFoundException.class);

    public PetNotFoundException(int id) {
        super("Could not find pet with id " + id);
        LOGGER.error("Could not find pet with id " + id);
    }
}
