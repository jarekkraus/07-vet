package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;

public class ConflictDateException extends Exception {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConflictDateException.class);

    public ConflictDateException(LocalDate date, LocalTime time) {
        super("Date: " + date.toString() + " at " + time.toString() + " is not available");
        LOGGER.error("Date: " + date.toString() + " at " + time.toString() + " is not available");
    }
}
