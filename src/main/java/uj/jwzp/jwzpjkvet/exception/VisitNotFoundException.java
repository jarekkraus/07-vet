package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VisitNotFoundException extends ResourceNotFoundException{

    private static final Logger LOGGER = LoggerFactory.getLogger(VisitNotFoundException.class);

    public VisitNotFoundException(int id) {
        super("Could not find visit with id " + id);
        LOGGER.error("Could not find visit with id " + id);
    }
}
