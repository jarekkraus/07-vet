package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class InvalidDateException extends Exception {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvalidDateException.class);

    public InvalidDateException(LocalDate date) {
        super("Date: " + date.toString() + " out of range");
        LOGGER.error("Date: " + date.toString() + " out of range");
    }
}
