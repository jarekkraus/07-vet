package uj.jwzp.jwzpjkvet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;

public class InvalidTimeException extends Exception{

    private static final Logger LOGGER = LoggerFactory.getLogger(InvalidTimeException.class);

    public InvalidTimeException(LocalTime timeStart, LocalTime timeEnd) {
        super("Time: " + timeStart.toString() + " - " + timeEnd.toString() + " is not valid");
        LOGGER.error("Time: " + timeStart.toString() + " - " + timeEnd.toString() + " is not valid");
    }
}
