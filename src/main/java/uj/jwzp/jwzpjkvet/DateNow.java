package uj.jwzp.jwzpjkvet;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DateNow {
    public LocalDate date() {
        return LocalDate.now();
    }
}
