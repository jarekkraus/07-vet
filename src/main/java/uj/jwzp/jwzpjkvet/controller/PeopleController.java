package uj.jwzp.jwzpjkvet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.jwzp.jwzpjkvet.model.Person;
import uj.jwzp.jwzpjkvet.service.PeopleService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class PeopleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeopleController.class);
    private final PeopleService peopleService;

    @Autowired
    public PeopleController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @GetMapping("/people")
    public ResponseEntity<List<Person>> getAllPeople() {
        LOGGER.info("requested to get all people");
        List<Person> people = peopleService.getAllPeople();
        LOGGER.debug("retrieving {} people", people);
        LOGGER.info("retrieving {} people", people.size());
        return new ResponseEntity<>(people, HttpStatus.OK);
    }

    @GetMapping("/people/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable(value = "id") int id,
                                            @RequestParam(required = false, defaultValue = "false") boolean withPets) {
        LOGGER.info("requested to get person id " + id + " with pets: " + withPets);
        Person person = withPets ? peopleService.getPersonWithPets(id) : peopleService.getPerson(id);
        LOGGER.debug("retrieving {} person", person);
        LOGGER.info("retrieving {} person ", 1);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @PostMapping("/people")
    public ResponseEntity<Person> addPerson(@Valid @RequestBody Person person) {
        LOGGER.info("requested to add person");
        LOGGER.debug("person to insert: {}", person);
        Person result = peopleService.addPerson(person);
        LOGGER.info("successfully added person");
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PutMapping("/people/{id}")
    public ResponseEntity<Person> updatePerson(@PathVariable(value = "id") int id,
                                              @Valid @RequestBody Person person) {
        LOGGER.info("requested to update person with id " + id);
        LOGGER.debug("person to update: {}", person);
        Person result = peopleService.updatePerson(id, person);
        LOGGER.info("successfully updated person");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/people/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable int id) {
        LOGGER.info("requested to delete person with id " + id);
        peopleService.deletePerson(id);
        LOGGER.info("successfully deleted person");
        return new ResponseEntity<>("Person with Id : " + id + " deleted successfully", HttpStatus.OK);
    }
}
