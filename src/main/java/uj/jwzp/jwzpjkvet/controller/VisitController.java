package uj.jwzp.jwzpjkvet.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.jwzp.jwzpjkvet.exception.ConflictDateException;
import uj.jwzp.jwzpjkvet.exception.InvalidDateException;
import uj.jwzp.jwzpjkvet.exception.InvalidTimeException;
import uj.jwzp.jwzpjkvet.model.*;
import uj.jwzp.jwzpjkvet.service.PeopleService;
import uj.jwzp.jwzpjkvet.service.PetsService;
import uj.jwzp.jwzpjkvet.service.VisitService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class VisitController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VisitController.class);
    private final VisitService visitService;
    private final PetsService petsService;
    private final PeopleService peopleService;

    @Autowired
    public VisitController(VisitService visitService, PetsService petsService, PeopleService peopleService) {
        this.visitService = visitService;
        this.petsService = petsService;
        this.peopleService = peopleService;
    }

    @GetMapping("/visits/{id}")
    public ResponseEntity<Visit> getVisit(@PathVariable(value = "id") int id) {
        LOGGER.info("requested to get visit with id " + id);
        Visit visit = visitService.getVisit(id);
        LOGGER.debug("retrieving {} visit", visit);
        LOGGER.info("retrieving {} visit ", 1);
        return new ResponseEntity<>(visit, HttpStatus.OK);
    }

    @GetMapping("/visits")
    public ResponseEntity<List<Visit>> getAllByDateWheresStatus(@RequestParam(value = "date") String date,
                                                          @RequestParam(value = "status", required = false, defaultValue = "APPOINTMENT") String status) {
        LOGGER.info("requested to get visits by date " + date);
        List<Visit> visits = visitService.getAllByDateWheresStatus(LocalDate.parse(date), VisitStatus.valueOf(status));
        LOGGER.debug("retrieving {} visits", visits);
        LOGGER.info("retrieving {} visits ", visits.size());
        return new ResponseEntity<>(visits, HttpStatus.OK);
    }

    @GetMapping("people/{id}/visits")
    public ResponseEntity<List<Visit>> getAllPersonVisits(@PathVariable(value = "id") int id) {
        LOGGER.info("requested to get visits by person id " + id);
        Person person = peopleService.getPerson(id);
        List<Visit> visits = visitService.getAllByPersonId(id);
        LOGGER.debug("retrieving {} visits", visits);
        LOGGER.info("retrieving {} visits ", visits.size());
        return new ResponseEntity<>(visits, HttpStatus.OK);
    }

    @JsonView(VisitView.ViewWithPet.class)
    @GetMapping("/pets/{id}/visits")
    public ResponseEntity<List<Visit>> getAllPetsVisits(@PathVariable(value = "id") int id) {
        LOGGER.info("requested to get visits by pet id " + id);
        Pet pet = petsService.getPet(id);
        List<Visit> visits = visitService.getAllByPetId(id);
        LOGGER.debug("retrieving {} visits", visits);
        LOGGER.info("retrieving {} visits ", visits.size());
        return new ResponseEntity<>(visits, HttpStatus.OK);
    }

    @JsonView(VisitView.ViewWithPet.class)
    @PostMapping("/pets/{id}/visits")
    public ResponseEntity<Visit> addVisit(@PathVariable(value = "id") int id,
                                          @Valid @RequestBody Visit visit)
            throws ConflictDateException, InvalidDateException, InvalidTimeException {
        LOGGER.info("requested to add visit");
        Pet pet = petsService.getPet(id);
        Visit newVisit = new Visit(
                visit.id(),
                pet,
                visit.date(),
                visit.timeStart(),
                visit.timeStart().plusMinutes(Long.parseLong(visit.duration())),
                visit.duration(),
                visit.description(),
                visit.status() == null ? VisitStatus.APPOINTMENT : visit.status()
        );
        LOGGER.debug("visit to insert: {}", newVisit);
        Visit result = visitService.addVisit(newVisit);
        LOGGER.info("successfully added visit");
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @JsonView(VisitView.ViewWithPet.class)
    @PatchMapping("/visits/{id}")
    public ResponseEntity<Visit> addDescriptionVisit(@PathVariable(value = "id") int id,
                                                     @RequestParam(value = "description") String description)
            throws ConflictDateException, InvalidDateException, InvalidTimeException {
        LOGGER.info("requested to add description visit with id " + id);
        Visit visit = visitService.getVisit(id);
        Visit newVisit = new Visit(
                visit.id(),
                visit.pet(),
                visit.date(),
                visit.timeStart(),
                visit.timeStart().plusMinutes(Long.parseLong(visit.duration())),
                visit.duration(),
                description,
                visit.status()
        );
        LOGGER.debug("description to add: {}", description);
        Visit result = visitService.addDescriptionVisit(newVisit);
        LOGGER.info("successfully added description");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
