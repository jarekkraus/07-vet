package uj.jwzp.jwzpjkvet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.service.PetsService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class PetsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PetsController.class);
    private final PetsService petsService;

    @Autowired
    public PetsController(PetsService petsService) {
        this.petsService = petsService;
    }

    @GetMapping("/pets")
    public ResponseEntity<List<Pet>> getAllPets() {
        LOGGER.info("requested to get all pets");
        List<Pet> pets = petsService.getAllPets();
        LOGGER.debug("retrieving {} pets", pets);
        LOGGER.info("retrieving {} pets", pets.size());
        return new ResponseEntity<>(pets, HttpStatus.OK);
    }

    @GetMapping("/pets/{id}")
    public ResponseEntity<Pet> getPet(@PathVariable(value = "id") int id) {
        LOGGER.info("requested to get pet with id " + id);
        Pet pet = petsService.getPet(id);
        LOGGER.debug("retrieving {} pet", pet);
        LOGGER.info("retrieving {} pet ", 1);
        return new ResponseEntity<>(pet, HttpStatus.OK);
    }

    @PostMapping("/pets")
    public ResponseEntity<Pet> addPet(@Valid @RequestBody Pet pet) {
        LOGGER.info("requested to add pet");
        LOGGER.debug("pet to insert: {}", pet);
        Pet result = petsService.addPet(pet);
        LOGGER.info("successfully added pet");
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PutMapping("/pets/{id}")
    public ResponseEntity<Pet> updatePet(@PathVariable(value = "id") int id,
                                              @Valid @RequestBody Pet pet) {
        LOGGER.info("requested to update pet with id " + id);
        LOGGER.debug("pet to update: {}", pet);
        Pet result = petsService.updatePet(id, pet);
        LOGGER.info("successfully updated pet");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/pets/{id}")
    public ResponseEntity<String> deletePet(@PathVariable int id) {
        LOGGER.info("requested to delete pet with id " + id);
        petsService.deletePet(id);
        LOGGER.info("successfully deleted pet");
        return new ResponseEntity<>("Pet with Id : " + id + " deleted successfully", HttpStatus.OK);
    }

    @PatchMapping("/pets/{id}")
    public ResponseEntity<String> assignOrRejectOwner(@PathVariable int id, @RequestParam int ownerId) {
        String message;
        if (ownerId == 0) {
            LOGGER.info("requested to reject pet with id " + id);
            petsService.rejectOwner(id);
            message = "Pet with Id : " + id + " reject person owner successfully";
        } else {
            LOGGER.info("requested to assign pet with id " + id + "to person with id " + ownerId);
            petsService.assignOwner(id, ownerId);
            message = "Pet with Id : " + id + " assign to person with id : " + ownerId + " successfully";
        }
        LOGGER.info(message);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
