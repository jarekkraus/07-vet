package uj.jwzp.jwzpjkvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uj.jwzp.jwzpjkvet.model.Person;

import java.util.List;
import java.util.Optional;

@Repository
public interface PeopleRepository extends JpaRepository<Person, Integer> {

    List<Person> findAll();

    Person save(Person person);

    Optional<Person> findById(Integer id);

    void deleteById(Integer id);
}
