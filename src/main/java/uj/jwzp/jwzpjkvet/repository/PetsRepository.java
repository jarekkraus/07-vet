package uj.jwzp.jwzpjkvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uj.jwzp.jwzpjkvet.model.Pet;

import java.util.List;
import java.util.Optional;

@Repository
public interface PetsRepository extends JpaRepository<Pet, Integer> {

    List<Pet> findAll();

    Pet save(Pet pet);

    Optional<Pet> findById(int id);

    void deleteById(int id);

    List<Pet> findAllByOwnerId(int ownerId);

    @Transactional
    @Modifying
    @Query("update pet set ownerId = ?2 where id = ?1")
    void assignOwner(int id, int ownerId);

    @Transactional
    @Modifying
    @Query("update pet set ownerId = null where id = ?1")
    void rejectOwner(int id);
}
