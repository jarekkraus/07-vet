package uj.jwzp.jwzpjkvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.Visit;
import uj.jwzp.jwzpjkvet.model.VisitStatus;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface VisitRepository extends JpaRepository<Visit, Integer> {

    Optional<Visit> findById(int id);

    List<Visit> getAllByPetId(int id);

    List<Visit> findAllByDate(LocalDate date);

    List<Visit> findAllByDateAndStatus(LocalDate date, VisitStatus status);

}
