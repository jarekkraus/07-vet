package uj.jwzp.jwzpjkvet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uj.jwzp.jwzpjkvet.DateNow;
import uj.jwzp.jwzpjkvet.exception.*;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.model.Visit;
import uj.jwzp.jwzpjkvet.model.VisitStatus;
import uj.jwzp.jwzpjkvet.repository.VisitRepository;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VisitService {

    private final DateNow dateNow;
    private final VisitRepository visitRepository;
    private final PetsService petsService;

    @Autowired
    public VisitService(DateNow dateNow, VisitRepository visitRepository, PetsService petsService) {
        this.dateNow = dateNow;
        this.visitRepository = visitRepository;
        this.petsService = petsService;
    }

    public Visit getVisit(int id) throws VisitNotFoundException {
        return visitRepository.findById(id)
                .orElseThrow(() -> new VisitNotFoundException(id));
    }

    public List<Visit> getAllByDate(LocalDate date) {
        return visitRepository.findAllByDate(date);
    }

    public List<Visit> getAllByDateWheresStatus(LocalDate date, VisitStatus status) {
        return visitRepository.findAllByDateAndStatus(date, status);
    }

    public List<Visit> getAllByPersonId(int id) {
        List<Pet> pets = petsService.findAllByOwnerId(id);
        List<Visit> visits = pets.stream()
                .map(pet -> getAllByPetId(pet.id())).flatMap(x -> x.stream())
                .collect(Collectors.toList());
        return visits;
    }

    public List<Visit> getAllByPetId(int id) {
        return visitRepository.getAllByPetId(id);
    }

    public Visit addVisit(Visit visit) throws ConflictDateException, InvalidDateException, InvalidTimeException {

        LocalDate date = dateNow.date();

        validDate(date, visit.date());
        validTime(visit.timeStart(), visit.timeEnd());

        if (isConflictedTermin(visit.date(), visit.timeStart(), visit.timeEnd())) {
            throw new ConflictDateException(visit.date(), visit.timeStart());
        }

        return visitRepository.save(visit);
    }

    public boolean isConflictedTermin(LocalDate date, LocalTime start, LocalTime end) {
        int size = getAllByDate(date).stream().filter(visit ->
                        (visit.timeStart().isAfter(start) && visit.timeStart().isBefore(end)) ||
                        (visit.timeEnd().isAfter(start) && visit.timeEnd().isBefore(end)) ||
                        (visit.timeStart().equals(start) && visit.timeEnd().equals(end))
            ).collect(Collectors.toList()).size();
        return size > 0;
    }

    public boolean validDate(LocalDate dateNow, LocalDate date) throws InvalidDateException {
        DayOfWeek day = date.getDayOfWeek();
        if (date.isAfter(dateNow) && day != DayOfWeek.SATURDAY && day != DayOfWeek.SUNDAY) {
            return true;
        }
        throw new InvalidDateException(date);
    }

    public boolean validTime(LocalTime timeStart, LocalTime timeEnd) throws InvalidTimeException {
        if (timeStart.isAfter(LocalTime.of(8,0)) &&
                timeEnd.isBefore(LocalTime.of(20,0))) {
            return true;
        }
        throw new InvalidTimeException(timeStart, timeEnd);
    }

    public Visit addDescriptionVisit(Visit newVisit) {
        return visitRepository.save(newVisit);
    }
}
