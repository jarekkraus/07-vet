package uj.jwzp.jwzpjkvet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import uj.jwzp.jwzpjkvet.exception.PetNotFoundException;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.repository.PetsRepository;

import java.util.List;

@Service
public class PetsService {

    private final PetsRepository petsRepository;
    private final PeopleService peopleService;

    @Autowired
    public PetsService(PetsRepository petsRepository, @Lazy PeopleService peopleService) {
        this.petsRepository = petsRepository;
        this.peopleService = peopleService;
    }

    public List<Pet> getAllPets() {
        return petsRepository.findAll();
    }

    public Pet getPet(int id) throws PetNotFoundException {
        return petsRepository.findById(id)
                .orElseThrow(() -> new PetNotFoundException(id));
    }

    public Pet addPet(Pet pet) {
        return petsRepository.save(pet);
    }

    public Pet updatePet(int id, Pet pet) throws PetNotFoundException {
        getPet(id);
        return petsRepository.save(pet);
    }

    public void deletePet(int id) throws PetNotFoundException {
        getPet(id);
        petsRepository.deleteById(id);
    }

    public void assignOwner(int id, int ownerId) throws PetNotFoundException {
        getPet(id);
        peopleService.getPerson(ownerId);
        petsRepository.assignOwner(id, ownerId);
    }

    public void rejectOwner(int id) throws PetNotFoundException {
        getPet(id);
        petsRepository.rejectOwner(id);
    }

    public List<Pet> findAllByOwnerId(int ownerId) {
        return petsRepository.findAllByOwnerId(ownerId);
    }

}
