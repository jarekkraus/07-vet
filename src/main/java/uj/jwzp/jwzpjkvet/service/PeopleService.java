package uj.jwzp.jwzpjkvet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uj.jwzp.jwzpjkvet.exception.PersonNotFoundException;
import uj.jwzp.jwzpjkvet.model.Person;
import uj.jwzp.jwzpjkvet.model.PersonWithPets;
import uj.jwzp.jwzpjkvet.model.Pet;
import uj.jwzp.jwzpjkvet.repository.PeopleRepository;

import java.util.List;

@Service
public class PeopleService {

    private final PeopleRepository peopleRepository;
    private final PetsService petsService;

    @Autowired
    public PeopleService(PeopleRepository peopleRepository, PetsService petsService) {
        this.peopleRepository = peopleRepository;
        this.petsService = petsService;
    }

    public List<Person> getAllPeople() {
        return peopleRepository.findAll();
    }

    public Person getPerson(int id) throws PersonNotFoundException {
        return peopleRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
    }

    public Person addPerson(Person person) {
        return peopleRepository.save(person);
    }

    public Person updatePerson(int id, Person person) throws PersonNotFoundException {
        getPerson(id);
        return peopleRepository.save(person);
    }

    public void deletePerson(int id) throws PersonNotFoundException {
        getPerson(id);
        peopleRepository.deleteById(id);
    }

    public Person getPersonWithPets(int id) throws PersonNotFoundException {
        Person person = getPerson(id);
        List<Pet> pets = petsService.findAllByOwnerId(id);
        PersonWithPets personWithPets = new PersonWithPets(person, pets);
        return personWithPets;
    }
}
