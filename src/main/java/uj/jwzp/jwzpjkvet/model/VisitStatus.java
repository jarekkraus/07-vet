package uj.jwzp.jwzpjkvet.model;

public enum VisitStatus {
    COMPLETED, NOTCAME, APPOINTMENT
}
