package uj.jwzp.jwzpjkvet.model;

import com.fasterxml.jackson.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity(name = "visit")
public class Visit {

    @JsonProperty("id")
    @JsonView(VisitView.ViewWithPet.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final Integer id;

    @JsonProperty("pet")
    @ManyToOne
    private final Pet pet;

    @JsonProperty("date")
    @JsonView(VisitView.ViewWithPet.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Date may not be empty")
    private final LocalDate date;

    @JsonProperty("timeStart")
    @JsonView(VisitView.ViewWithPet.class)
    @DateTimeFormat(pattern = "HH:mm:ss")
    @NotNull(message = "Time may not be empty")
    private final LocalTime timeStart;

    @JsonProperty("timeEnd")
    @JsonView(VisitView.ViewWithPet.class)
    private final LocalTime timeEnd;

    @JsonProperty("duration")
    @JsonView(VisitView.ViewWithPet.class)
    @Pattern(regexp = "^15$|^30$|^45$|^60$")
    @NotNull(message = "Duration may not be empty")
    private final String duration;

    @JsonProperty("description")
    @JsonView(VisitView.ViewWithPet.class)
    private final String description;

    @JsonProperty(value = "status")
    @JsonView(VisitView.ViewWithPet.class)
    @Enumerated(EnumType.STRING)
    private final VisitStatus status;

    public Visit() {
        this.id = 0;
        this.pet = null;
        this.date = null;
        this.timeStart = null;
        this.timeEnd = null;
        this.duration = null;
        this.description = null;
        this.status = null;
    }

    public Visit(Integer id, Pet pet,
                 @NotNull(message = "Date may not be empty") LocalDate date,
                 @NotNull(message = "Time may not be empty") LocalTime timeStart,
                 LocalTime timeEnd, @Pattern(regexp = "^15$|^30$|^45$|^60$")
                 @NotNull(message = "Duration may not be empty") String duration,
                 String description,
                 VisitStatus status) {
        this.id = id;
        this.pet = pet;
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.duration = duration;
        this.description = description;
        this.status = status;
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)


    public Integer id() {
        return id;
    }

    public Pet pet() {
        return pet;
    }

    public LocalDate date() {
        return date;
    }

    public LocalTime timeStart() {
        return timeStart;
    }

    public LocalTime timeEnd() {
        return timeEnd;
    }

    public String duration() {
        return duration;
    }

    public String description() {
        return description;
    }

    public VisitStatus status() {
        return status;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "id=" + id +
                ", pet=" + pet +
                ", date=" + date +
                ", timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                ", duration='" + duration + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                '}';
    }
}
