package uj.jwzp.jwzpjkvet.model;

public enum PetType {
    DOG,
    CAT,
    HAMSTER,
    HORSE
}