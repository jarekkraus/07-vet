package uj.jwzp.jwzpjkvet.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity(name = "person")
public class Person {

    @JsonProperty("id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final Integer id;

    @JsonProperty("firstName")
    @NotEmpty(message = "FirstName may not be empty")
    private final String firstName;

    @JsonProperty("lastName")
    @NotEmpty(message = "LastName may not be empty")
    private final String lastName;

    @JsonProperty("address")
    @NotEmpty(message = "Address may not be empty")
    private final String address;

    @JsonProperty("telephone")
    @Digits(fraction = 0, integer = 10)
    @NotEmpty(message = "Telephone may not be empty")
    private final String telephone;

    @JsonProperty("email")
    @Email
    @NotEmpty(message = "Email may not be empty")
    private final String email;

    public Person() {
        this.id = 0;
        this.firstName = null;
        this.lastName = null;
        this.address = null;
        this.telephone = null;
        this.email = null;
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public Person(Integer id,
                  @NotEmpty(message = "FirstName may not be empty") String firstName,
                  @NotEmpty(message = "LastName may not be empty") String lastName,
                  @NotEmpty(message = "Address may not be empty") String address,
                  @Digits(fraction = 0, integer = 10) @NotEmpty(message = "Telephone may not be empty") String telephone,
                  @Email @NotEmpty(message = "Email may not be empty") String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.telephone = telephone;
        this.email = email;
    }

    public Integer id() {
        return id;
    }

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    public String address() {
        return address;
    }

    public String telephone() {
        return telephone;
    }

    public String email() {
        return email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
