package uj.jwzp.jwzpjkvet.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity(name = "pet")
public class Pet {

    @JsonProperty("id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final Integer id;

    @JsonProperty("name")
    @NotEmpty(message = "Name may not be empty")
    private final String name;

    @JsonProperty("petType")
    @Enumerated(EnumType.STRING)
    @NotNull
    private final PetType petType;

    @JsonProperty("birthDate")
    @NotEmpty(message = "BirthDate may not be empty")
    @Pattern(regexp = "([12]\\d{3}(-(0[1-9]|1[0-2])(-(0[1-9]|[12]\\d|3[01]))*)*)",
            message = "correct format: yyyy, yyyy-MM, yyyy-MM-dd")
    private final String birthDate;

    @JsonProperty("deathDate")
    @Pattern(regexp = "([12]\\d{3}(-(0[1-9]|1[0-2])(-(0[1-9]|[12]\\d|3[01]))*)*)",
            message = "correct format: yyyy, yyyy-MM, yyyy-MM-dd"
    )
    private final String deathDate;

    @JsonProperty("ownerId")
    private final Integer ownerId;

    @OneToMany(mappedBy = "pet", cascade = CascadeType.REMOVE)
    private final List<Visit> visitList;

    public Pet() {
        this.id = 0;
        this.name = null;
        this.petType = null;
        this.birthDate = null;
        this.deathDate = null;
        this.ownerId = null;
        this.visitList = null;
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public Pet(Integer id,
               @NotEmpty(message = "Name may not be empty") String name,
               @NotEmpty(message = "PetType may not be empty") PetType petType,
               @NotEmpty(message = "BirthDate may not be empty") String birthDate, String deathDate, Integer ownerId, List<Visit> visitList) {
        this.id = id;
        this.name = name;
        this.petType = petType;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.ownerId = ownerId;
        this.visitList = visitList;
    }

    public Integer id() {
        return id;
    }

    public String name() {
        return name;
    }

    public PetType petType() {
        return petType;
    }

    public String birthDate() {
        return birthDate;
    }

    public String deathDate() {
        return deathDate;
    }

    public Integer ownerId() {
        return ownerId;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", petType=" + petType +
                ", birthDate='" + birthDate + '\'' +
                ", deathDate='" + deathDate + '\'' +
                ", ownerId=" + ownerId +
                '}';
    }

}
