package uj.jwzp.jwzpjkvet.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PersonWithPets extends Person {

    @JsonProperty("pets")
    private final List<Pet> pets;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public PersonWithPets(Person person, List<Pet> pets) {
        super(person.id(), person.firstName(), person.lastName(), person.address(), person.telephone(), person.email());
        this.pets = List.copyOf(pets);
    }

    public List<Pet> pets() {
        return pets;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + super.id() +
                ", firstName='" + super.firstName() + '\'' +
                ", lastName='" + super.lastName() + '\'' +
                ", address='" + super.address() + '\'' +
                ", telephone='" + super.telephone() + '\'' +
                ", email='" + super.email() + '\'' +
                ", pets=" + pets +
                '}';
    }
}
